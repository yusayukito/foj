﻿#pragma once
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampScript : MonoBehaviour
{
    private Animator anim = null;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Lamp()
    {
        anim.SetBool("lamp", true);
        anim.SetBool("chance", false);
    }
    public void Chance()
    {
        anim.SetBool("chance", true);
        anim.SetBool("lamp", false);
    }
}
