﻿#pragma once
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiddleScript : MonoBehaviour
{
    private Animator anim = null;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void MiddleRoll()      //リール回転
    {
        anim.SetBool("roll", true);
        anim.SetBool("big1", false);
        anim.SetBool("big2", false);
        anim.SetBool("rep1", false);
        anim.SetBool("rep2", false);
        anim.SetBool("che1", false);
        anim.SetBool("che2", false);
        anim.SetBool("app1", false);
        anim.SetBool("app2", false);
        anim.SetBool("bel1", false);
        anim.SetBool("bel2", false);
        anim.SetBool("los1", false);
        anim.SetBool("los2", false);
        anim.SetBool("los3", false);
        anim.SetBool("los4", false);
    }
    public void Big1()
    {
        anim.SetBool("big1", true);
        anim.SetBool("roll", false);
    }
    public void Big2()
    {
        anim.SetBool("big2", true);
        anim.SetBool("roll", false);
    }
    public void Rep1()
    {
        anim.SetBool("rep1", true);
        anim.SetBool("roll", false);
    }
    public void Rep2()
    {
        anim.SetBool("rep2", true);
        anim.SetBool("roll", false);
    }
    public void Che1()
    {
        anim.SetBool("che1", true);
        anim.SetBool("roll", false);
    }
    public void Che2()
    {
        anim.SetBool("che2", true);
        anim.SetBool("roll", false);
    }
    public void App1()
    {
        anim.SetBool("app1", true);
        anim.SetBool("roll", false);
    }
    public void App2()
    {
        anim.SetBool("app2", true);
        anim.SetBool("roll", false);
    }
    public void Bel1()
    {
        anim.SetBool("bel1", true);
        anim.SetBool("roll", false);
    }
    public void Bel2()
    {
        anim.SetBool("bel2", true);
        anim.SetBool("roll", false);
    }
    public void Los1()
    {
        anim.SetBool("los1", true);
        anim.SetBool("roll", false);
    }
    public void Los2()
    {
        anim.SetBool("los2", true);
        anim.SetBool("roll", false);
    }
    public void Los3()
    {
        anim.SetBool("los3", true);
        anim.SetBool("roll", false);
    }
    public void Los4()
    {
        anim.SetBool("los4", true);
        anim.SetBool("roll", false);
    }
}
