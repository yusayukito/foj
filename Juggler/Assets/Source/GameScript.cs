﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InputKey;

namespace InputKey
{
    //同時入力の阻止
    public static class MyInput
    {
        static bool isCheck_Input;

        //リールボタン押している時は他のボタンを反応させない
        public static bool MyInputKey(KeyCode key)
        {
            if (Input.anyKey == false) isCheck_Input = false;

            if (isCheck_Input == false)
            {
                if (Input.GetKey(key))
                {
                    isCheck_Input = true;
                    return true;
                }
            }
            return false;
        }
    }
}

public class GameScript : MonoBehaviour
{
    GameObject Reel_Left;
    LeftScript leftScript;          //左リール

    GameObject Reel_Middle;
    MiddleScript middleScript;      //中リール

    GameObject Reel_Right;
    RightScript rightScript;        //右リール

    GameObject Lamp;
    LampScript lampScript;          //ランプ

    GameObject DataCounter;
    DataScript dataScript;          //データカウンター

    GameObject TotalData;
    TotalScript totalScript;        //トータルデータ

    public int value = 0;
    public int big = 1;
    public int judge = 1;
    public int lost = 0;
    public int key1 = 1;            //左リールが回転している
    public int key2 = 1;            //中リールが回転している
    public int key3 = 1;            //右リールが回転している
    public int key4 = 1;            //リールバグ対策
    public int key5 = 1;            //データカウンター重複阻止
    public int key6 = 0;            //リプレイ停止時次回転投入枚数を0
    public int key7 = 24;           //BIGに当選しているか
    public int key8 = 8;            //REGに当選しているか


    public GameObject count_object = null;      //Textオブジェクト
    public GameObject big_object = null;
    public GameObject reg_object = null;
    public GameObject totalcount_object = null;
    public GameObject inputcoin_object = null;
    public GameObject getcoin_object = null;
    public GameObject totalcoin_object = null;

    public int count_num = 0;                   //回転数
    public int big_num = 0;                     //BIGの当選数
    public int reg_num = 0;                     //REGの当選数
    public int totalcount_num = 0;              //総回転数
    public int inputcoin_num = 0;               //総投入枚数
    public int getcoin_num = 0;                 //総獲得枚数
    public int totalcoin_num = 0;               //総差枚数

    // Start is called before the first frame update
    void Start()
    {
        Reel_Left = GameObject.Find("Reel_Left");
        leftScript = Reel_Left.GetComponent<LeftScript>();              //左リール

        Reel_Middle = GameObject.Find("Reel_Middle");
        middleScript = Reel_Middle.GetComponent<MiddleScript>();        //中リール

        Reel_Right = GameObject.Find("Reel_Right");
        rightScript = Reel_Right.GetComponent<RightScript>();           //右リール

        Lamp = GameObject.Find("Lamp");
        lampScript = Lamp.GetComponent<LampScript>();                   //ランプ

        DataCounter = GameObject.Find("DataCounter");
        dataScript = DataCounter.GetComponent<DataScript>();            //データカウンター

        TotalData = GameObject.Find("TotalData");
        totalScript = TotalData.GetComponent<TotalScript>();            //トータルデータ
    }

    // Update is called once per frame
    void Update()
    {
        Text count_text = count_object.GetComponent<Text>();    //オブジェクトからTextコンポーネントを取得
        Text big_text = big_object.GetComponent<Text>();
        Text reg_text = reg_object.GetComponent<Text>();
        Text totalcount_text = totalcount_object.GetComponent<Text>();
        Text inputcoin_text = inputcoin_object.GetComponent<Text>();
        Text getcoin_text = getcoin_object.GetComponent<Text>();
        Text totalcoin_text = totalcoin_object.GetComponent<Text>();

        count_text.text = "" + count_num;                       //テキスト表示入れ替え、文字列入れないとエラー？
        big_text.text = "" + big_num;
        reg_text.text = "" + reg_num;
        totalcount_text.text = "" + totalcount_num;
        inputcoin_text.text = "" + inputcoin_num;
        getcoin_text.text = "" + getcoin_num;
        totalcoin_text.text = "" + totalcoin_num;

        if (key1 * key2 * key3 == 1)            //全てのリールが止まっているか
        {
            if (MyInput.MyInputKey(KeyCode.UpArrow))    //リールを回した時の処理
            {
                lampScript.Lamp();              //ランプ消灯

                leftScript.LeftRoll();          //左リール回転
                middleScript.MiddleRoll();      //中リール回転
                rightScript.RightRoll();        //右リール回転

                dataScript.Roll();              //データカウンターの停止
                totalScript.Roll();             //トータルデータの停止
                if (key7 < 23 || key8 < 7)
                {
                    dataScript.Hit();           //データカウンターの回転  
                    totalScript.Hit();          //トータルデータの回転
                }

                value = Random.Range(0, 2);     //２通りの当たり方をランダムで
                big = Random.Range(0, 100);     //BIGの抽選
                judge = Random.Range(0, 150);   //その他の抽選
                lost = Random.Range(0, 4);      //４通りの外れ方をランダムで

                if (key7 < 23 || key8 < 7)
                {
                    big = 1;
                    judge = 28;                 //BIGかREG当選中出目をappleに
                }

                key1 = 0;                       //左リールが回転している
                key2 = 0;                       //中リールが回転している
                key3 = 0;                       //右リールが回転している
                key4 = 0;                       //リールバグ対策
                key5 = 0;                       //データカウンター重複阻止
                
                if (big == 0 || judge == 0)     //BIGまたはREG当選時ランプ点灯           
                {
                    lampScript.Chance();
                }


                if (key6 == 0)
                {
                    if (key7 < 23 || key8 < 7)
                    {
                        inputcoin_num += 2;         //BIGかREG中総投入枚数に2加算
                        totalcoin_num -= 2;         //BIGかREG中総差枚数から2減算  
                    }
                    else
                    {
                        inputcoin_num += 3;         //総投入枚数に3加算
                        totalcoin_num -= 3;         //総差枚数から3減算        
                    }
                }
                else if (key6 == 1)              //リプレイ停止時次回転投入枚数を0
                {
                    key6 = 0;
                }

                if(key7 == 0 || key7 == 23 || key8 == 0 || key8 == 7)       //BIGかREG当選時と終了時回転数を0
                {
                    count_num = 0;
                    if (key7 == 23 || key8 == 7)
                    {
                        key7 += 1;
                        key8 += 1;
                    }
                }
                count_num += 1;                 //回転数に1加算
                totalcount_num += 1;            //総回転数に1加算
            }
        }



        if (big == 0)//BIG当選時---------------------------------
        {
            //左リール----------------------
            if (value == 0)
            {
                if (MyInput.MyInputKey(KeyCode.LeftArrow))
                {
                    leftScript.Big1();
                    key1 = 1;
                }
            }
            else if (value == 1)
            {
                if (MyInput.MyInputKey(KeyCode.LeftArrow))
                {
                    leftScript.Big2();
                    key1 = 1;
                }
            }

            //中リール---------------------
            if (value == 0)
            {
                if (MyInput.MyInputKey(KeyCode.DownArrow))
                {
                    middleScript.Big1();
                    key2 = 1;
                }
            }
            else if (value == 1)
            {
                if (MyInput.MyInputKey(KeyCode.DownArrow))
                {
                    middleScript.Big2();
                    key2 = 1;
                }
            }

            //右リール----------------------
            if (value == 0)
            {
                if (MyInput.MyInputKey(KeyCode.RightArrow))
                {
                    rightScript.Big1();
                    key3 = 1;
                }
            }
            else if (value == 1)
            {
                if (MyInput.MyInputKey(KeyCode.RightArrow))
                {
                    rightScript.Big2();
                    key3 = 1;
                }
            }

            //他二つのリールが止まっているならBIGに1加算し回転数をリセット
            if (key4 == 1) {
                if (key5 == 0)
                { 
                    if (Input.GetKeyUp("left"))
                    {
                        big_num += 1;
                        key5 = 1;
                        key7 = 0;
                    }
                    if (Input.GetKeyUp("down"))
                    {
                        big_num += 1;
                        key5 = 1;
                        key7 = 0;
                    }
                    if (Input.GetKeyUp("right"))
                    {
                        big_num += 1;
                        key5 = 1;
                        key7 = 0;
                    }
                }
            }
        }//------------------------------------------------



        else if (big != 0)//BIG非当選時-------------------------------
        {
            if (judge == 0)//REGの当選時-----------------
            {
                //左リール----------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Big1();
                        key1 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Big2();
                        key1 = 1;
                    }
                }
                //中リール---------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Big1();
                        key2 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Big2();
                        key2 = 1;
                    }
                }

                //右リール-----------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Reg1();
                        key3 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Reg2();
                        key3 = 1;
                    }
                }

                //他二つのリールが止まっているならREGに1加算し回転数をリセット
                if (key4 == 1)
                {
                    if (key5 == 0)
                    {
                        if (Input.GetKeyUp("left"))
                        {
                            reg_num += 1;
                            key5 = 1;
                            key8 = 0;
                        }
                        if (Input.GetKeyUp("down"))
                        {
                            reg_num += 1;
                            key5 = 1;
                            key8 = 0;
                        }
                        if (Input.GetKeyUp("right"))
                        {
                            reg_num += 1;
                            key5 = 1;
                            key8 = 0;
                        }
                    }
                }
            }//----------------------------------------



            else if (judge <= 22)//Replay当選時--------------------------
            {
                //左リール----------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Rep1();
                        key1 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Rep2();
                        key1 = 1;
                    }
                }
                //中リール---------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Rep1();
                        key2 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Rep2();
                        key2 = 1;
                    }
                }

                //右リール-----------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Rep1();
                        key3 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Rep2();
                        key3 = 1;
                    }
                }

                //リプレイ停止時次回転投入枚数を0
                if (key4 == 1)
                {
                    if (key5 == 0)
                    {
                        if (Input.GetKeyUp("left"))
                        {
                            key5 = 1;
                            key6 = 1;
                        }
                        if (Input.GetKeyUp("down"))
                        {
                            key5 = 1;
                            key6 = 1;
                        }
                        if (Input.GetKeyUp("right"))
                        {
                            key5 = 1;
                            key6 = 1;
                        }
                    }
                }
            }//----------------------------------------



            else if (judge <= 27)//Cherry当選時-----------------------
            {
                //左リール----------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Che1();
                        key1 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Che2();
                        key1 = 1;
                    }
                }
                //中リール---------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Che1();
                        key2 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Che2();
                        key2 = 1;
                    }
                }

                //右リール-----------------------

                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Che1();
                        key3 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Che2();
                        key3 = 1;
                    }
                }

                //コインの処理
                if (key4 == 1)
                {
                    if (key5 == 0)
                    {
                        if (Input.GetKeyUp("left"))
                        {
                            getcoin_num += 1;
                            totalcoin_num += 1;
                            key5 = 1;
                        }
                        if (Input.GetKeyUp("down"))
                        {
                            getcoin_num += 1;
                            totalcoin_num += 1;
                            key5 = 1;
                        }
                        if (Input.GetKeyUp("right"))
                        {
                            getcoin_num += 1;
                            totalcoin_num += 1;
                            key5 = 1;
                        }
                    }
                }
            }//----------------------------------------



            else if (judge <= 52)//Apple当選時-----------------------
            {
                //左リール----------------------
                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.App1();
                        key1 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.App2();
                        key1 = 1;
                    }
                }

                //中リール---------------------
                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.App1();
                        key2 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.App2();
                        key2 = 1;
                    }
                }

                //右リール----------------------
                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.App1();
                        key3 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.App2();
                        key3 = 1;
                    }
                }

                //コインの処理
                if (key4 == 1)
                {
                    if (key5 == 0)
                    {
                        if (Input.GetKeyUp("left"))
                        {
                            if (key7 < 23 || key8 < 7)
                            {
                                getcoin_num += 15;
                                totalcoin_num += 15;
                                key7 += 1;
                                key8 += 1;
                            }
                            else
                            {
                                getcoin_num += 7;
                                totalcoin_num += 7;
                            }
                            key5 = 1;
                        }
                        if (Input.GetKeyUp("down"))
                        {
                            if (key7 < 23 || key8 < 7)
                            {
                                getcoin_num += 15;
                                totalcoin_num += 15;
                                key7 += 1;
                                key8 += 1;
                            }
                            else
                            {
                                getcoin_num += 7;
                                totalcoin_num += 7;
                            }
                            key5 = 1;
                        }
                        if (Input.GetKeyUp("right"))
                        {
                            if (key7 < 23 || key8 < 7)
                            {
                                getcoin_num += 15;
                                totalcoin_num += 15;
                                key7 += 1;
                                key8 += 1;
                            }
                            else
                            {
                                getcoin_num += 7;
                                totalcoin_num += 7;
                            }
                            key5 = 1;
                        }
                    }
                }
            }//----------------------------------------



            else if (judge <= 53)//Bell当選時-----------------------
            {
                //左リール----------------------
                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Bel1();
                        key1 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Bel2();
                        key1 = 1;
                    }
                }

                //中リール---------------------
                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Bel1();
                        key2 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Bel2();
                        key2 = 1;
                    }
                }

                //右リール----------------------
                if (value == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Bel1();
                        key3 = 1;
                    }
                }
                else if (value == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Bel2();
                        key3 = 1;
                    }
                }

                //コインの処理
                if (key4 == 1)
                {
                    if (key5 == 0)
                    {
                        if (Input.GetKeyUp("left"))
                        {
                            getcoin_num += 15;
                            totalcoin_num += 15;
                            key5 = 1;
                        }
                        if (Input.GetKeyUp("down"))
                        {
                            getcoin_num += 15;
                            totalcoin_num += 15;
                            key5 = 1;
                        }
                        if (Input.GetKeyUp("right"))
                        {
                            getcoin_num += 15;
                            totalcoin_num += 15;
                            key5 = 1;
                        }
                    }
                }
            }//----------------------------------------



            else if (judge <= 149)//ハズレ目---------------------------
            {
                //左リール----------------------
                if (lost == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Los1();
                        key1 = 1;
                    }
                }
                else if (lost == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Los2();
                        key1 = 1;
                    }
                }
                else if (lost == 2)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Los3();
                        key1 = 1;
                    }
                }
                else if (lost == 3)
                {
                    if (MyInput.MyInputKey(KeyCode.LeftArrow))
                    {
                        leftScript.Los4();
                        key1 = 1;
                    }
                }

                //中リール---------------------

                if (lost == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Los1();
                        key2 = 1;
                    }
                }
                else if (lost == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Los2();
                        key2 = 1;
                    }
                }
                else if (lost == 2)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Los3();
                        key2 = 1;
                    }
                }
                else if (lost == 3)
                {
                    if (MyInput.MyInputKey(KeyCode.DownArrow))
                    {
                        middleScript.Los4();
                        key2 = 1;
                    }
                }

                //右リール-----------------------

                if (lost == 0)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Los1();
                        key3 = 1;
                    }
                }
                else if (lost == 1)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Los2();
                        key3 = 1;
                    }
                }
                else if (lost == 2)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Los3();
                        key3 = 1;
                    }
                }
                else if (lost == 3)
                {
                    if (MyInput.MyInputKey(KeyCode.RightArrow))
                    {
                        rightScript.Los4();
                        key3 = 1;
                    }
                }
            }//----------------------------------------
        }
        if((key1 + key2 + key3) == 3)
        {
            key4 = 1;
        }
        //リールバグ対策
        if (key4 == 1)
        {
            if (Input.GetKey("left"))
            {
                key1 = 0;
            }
            if (Input.GetKey("down"))
            {
                key2 = 0;
            }
            if (Input.GetKey("right"))
            {
                key3 = 0;
            }
        }
        //全リールの停止確認
        if (key4 == 1)
        {
            if (Input.GetKeyUp("left"))
            {
                key1 = 1;
            }
            if (Input.GetKeyUp("down"))
            {
                key2 = 1;
            }
            if (Input.GetKeyUp("right"))
            {
                key3 = 1;
            }
        }
    }
}
