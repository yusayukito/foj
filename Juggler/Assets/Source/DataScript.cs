#pragma once
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataScript : MonoBehaviour
{
    private Animator anim = null;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void Roll()
    {
        anim.SetBool("roll", false);
    }
    public void Hit()
    {
        anim.SetBool("roll", true);
    }
}